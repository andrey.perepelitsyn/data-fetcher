'use strict';

const debug = require('debug')('data-loader');
const commander = require('commander');
const byline = require('byline');
const xz = require('xz');
const fs = require('fs');
const path = require('path');
const zlib = require('zlib')
const rowFormatter = require('./row-formatter');
const Writer = require('./writer-db');

function parseCmdOptions() {
	function dateArgParser(arg) {
		const date = new Date(arg + 'Z');
		if(isNaN(date.valueOf()))
			throw new commander.InvalidOptionArgumentError('wrong date');
		return date.valueOf();
	}

	const program = new commander.Command();
	program.version('0.1');

	program
		.addOption(new commander.Option('-s, --storage-path <path>', 'path to load data from')
			.default('.'))
		.addOption(new commander.Option('-i, --instrument <name>', 'instrument name')
			.makeOptionMandatory())
		.addOption(new commander.Option('--store-instrument <name>', 'instrument name to store, if differs from original'))
		.addOption(new commander.Option('-f, --from <datetime>', 'request data from this date and time. if not set -- lookup last bar in DB')
			.argParser(dateArgParser))
		.addOption(new commander.Option('-t, --to <datetime>', 'request data to this date and time')
			.argParser(dateArgParser)
			.default(Date.now()))
		.addOption(new commander.Option('--ignore-gap', 'igonre time gap between last data in DB and first data in files'))
		.addOption(new commander.Option('--db-host <host>', 'DB connection host')
			.default('127.0.0.1'))
		.addOption(new commander.Option('--db-port <port>', 'DB connection port')
			.default('3306'))
		.addOption(new commander.Option('--db-name <name>', 'DB scheme name')
			.makeOptionMandatory())
		.addOption(new commander.Option('--db-username <name>', 'DB user\'s name')
			.makeOptionMandatory())
		.addOption(new commander.Option('--db-password <password>', 'DB user\'s password')
			.makeOptionMandatory());

	program.parse(process.argv);
	return program.opts();
}

// return null if ts is earlier than first file
function getFileIndexByTimestamp(fileList, ts) {
	debug('getFileIndexByTimestamp(), arguments:', arguments);
	for(let i = 0; i < fileList.length; i++) {
		if(ts < fileList[i].startsAt)
			if(i == 0)
				return null;
			else
				return i - 1;
	}
	return fileList.length - 1;
}

main();

async function main() {
	try {
		const options = parseCmdOptions();
		options.storeInstrument = options.storeInstrument ?? options.instrument;
		debug('running with options:', options);

		const inputFiles = (await fs.promises.readdir(options.storagePath, { withFileTypes: true }))
			.filter(entry => entry.isFile())
			.map(entry => ({
				name: path.join(options.storagePath, entry.name),
				startsAt: parseInt(/\d{13}/.exec(entry.name))
			}));
		inputFiles.sort((a, b) => a.startsAt - b.startsAt);

		const dbOptions = {
			host: options.dbHost,
			port: options.dbPort,
			database: options.dbName,
			user: options.dbUsername,
			password: options.dbPassword
		};
		const obWriter = new Writer(dbOptions, 'ob_'+options.storeInstrument,
			['seq', 'datetime', 'type', 'data']);
		await obWriter.init();
		const tickWriter = new Writer(dbOptions, 'ticks_'+options.storeInstrument,
			['id', 'price', 'quantity', 'direction', 'seq', 'datetime']);
		await tickWriter.init();

		if(!options.from) {
			debug('--from not defined, trying to guess best');
			const [ lastObTime, lastTickTime ] = await Promise.all([
				(async () => {
					const lastRow = await obWriter.peekLastRow(options.instrument);
					return lastRow && lastRow.datetime;
				})(),
				(async () => {
					const lastRow = await tickWriter.peekLastRow(options.instrument);
					return lastRow && lastRow.datetime;
				})()
			]);
			debug('lastObTime:', lastObTime);
			debug('lastTickTime:', lastTickTime);
			if(lastObTime ? !lastTickTime : lastTickTime) {
				throw new Error('empty orderbook table XOR empty ticks table. it is wrong!');
			}
			if(lastObTime) {
				options.from = lastObTime.valueOf() + 1;
			}
			else {
				options.from = inputFiles[0].startsAt;
				debug('no --from date and no data in DB. loading from first available file');
			}
		}

		if(!options.to)
			options.to = 1e13; // should be enough for maximum possible date, since we assume ts has 13 digits

		if(options.from > options.to)
			throw new Error('--from date is greate than --to date');

		options.fromFileIndex = getFileIndexByTimestamp(inputFiles, options.from);
		options.toFileIndex =   getFileIndexByTimestamp(inputFiles, options.to);

		debug('running with options:', options);
		debug('running\nfrom\t%s\nto\t%s', new Date(options.from).toISOString(), new Date(options.to).toISOString());

		if(options.fromFileIndex === null && options.ignoreGap)
			options.fromFileIndex = 0;
		
		if(options.fromFileIndex === null || options.toFileIndex === null)
			throw new Error('requested range is not covered by data files');

		for(let i = options.fromFileIndex; i <= options.toFileIndex; i++) {
			const fileName = inputFiles[i].name;
			debug('opening file', fileName);
			let input;
			if(fileName.slice(-3) == '.xz') {
				const fileStream = fs.createReadStream(fileName);
				input = new xz.Decompressor();
				fileStream.pipe(input);
			}
			else if(fileName.slice(-3) == '.gz') {
				const fileStream = fs.createReadStream(fileName);
				input = zlib.createGunzip();
				fileStream.pipe(input);
			}
			else if(fileName.slice(-3) == '.br') {
				const fileStream = fs.createReadStream(fileName);
				input = zlib.createBrotliDecompress();
				fileStream.pipe(input);
			}
			else {
				input = fs.createReadStream(fileName, { encoding: 'utf8' });
			}
			for await (const lineBuf of byline(input)) {
				const line = lineBuf.toString();
				const [ tsStr, connector, instrument, data ] = line.split('\t');
				const ts = parseInt(tsStr);

				if(ts < options.from) {
					// debug('skipping old data: %d < %d', ts, options.from);
					continue;
				}
				if(ts > options.to) {
					debug('skipping rest of data');
					break;
				}

				if(instrument.slice(0,2) == '==') {
					debug('skipping service data:', line);
					continue;
				}

				const obRow = rowFormatter[connector].orderbook(ts, instrument, data);
				if(obRow)
					await obWriter.write(obRow);
				else {
					const tickRows = rowFormatter[connector].tick(ts, instrument, data);
					if(tickRows)
						for(const tickRow of tickRows)
							await tickWriter.write(tickRow);
					else
						debug('unknown data format:', line);
				}
			}
		}
		await Promise.all([obWriter.close(), tickWriter.close()]);
		debug('writers closed');
	} catch(e) {
		console.error(e);
		process.exit(1);
	}
}