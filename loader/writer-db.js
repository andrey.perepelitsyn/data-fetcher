'use strict';
const debug = require('debug')('writer_db');
const mysql = require('promise-mysql');

function objectToRow(o) {
	return [o.instrument, o.datetime, o.open, o.high, o.low, o.close, o.volume];
}

function datetimeCast(field, next) {
	if(field.type === 'DATETIME') {
		return new Date(field.string() + 'Z');
	} else
		return next();
}

class WriterDb {
	constructor(mysqlOptions, tableName, colNames, chunkSize = 1000) {
		this.mysqlOptions = Object.assign({typeCast: datetimeCast}, mysqlOptions);
		this.accum = [];
		this.rowCount = 0;
		this.chunkSize = chunkSize;
		this.db = null;
		this.tableName = tableName;
		this.colNames = colNames;
		this.sqlQuery = `replace into \`${tableName}\` (${colNames.join(', ')}) values ?`;
	}

	async init() {
		this.db = await mysql.createConnection(this.mysqlOptions);
	}

	async write(data) {
		this.accum.push(data);

		if(this.accum.length >= this.chunkSize)
			await this.flush();
	}

	async flush() {
		if(this.accum.length == 0)
			return this.rowCount;

		if(this.db == null)
			throw new Error('not connected to DB');

		const query = this.db.format(this.sqlQuery, [this.accum]);
		// debug('query:', query);
		const result = await this.db.query(query);

		debug('chunk written, affected', result.affectedRows, 'rows, changed', result.changedRows);
		this.rowCount += this.accum.length;
		this.accum = [];
		debug('this.rowCount =', this.rowCount);
		return this.rowCount;
	}

	async peekLastRow(instrument) {
		const result = await this.db.query(
			`select * from \`${this.tableName}\` order by datetime desc limit 1`, instrument);
		if(result.length != 1)
			return null;
		debug(result);
		return result[0];
	}

	async close() {
		this.flush();
		if(this.db)
			await this.db.end();
	}
};

module.exports = WriterDb;
