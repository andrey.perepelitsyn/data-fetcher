'use strict';

const debug = require('debug')('row-formatter');

// orderbook<exchange> returns [ seq, datetime, type('snapshot', 'update'), data ]
// tick<exchange> returns Array of [ id, price, quantity, direction('Buy', 'Sell'), seq, datetime ]
// ...or null if data doesn't fit the format

function orderbookBitmex(fetchTs, instrument, data) {
	if(data.slice(0, 22) == '{"table":"orderBookL2"') {
		const type = data.slice(23, 41) == '"action":"partial"' ? 'snapshot' : 'update';
		return [ fetchTs, new Date(fetchTs), type, data ];
	}
	return null;
}

function tickBitmex(fetchTs, instrument, data) {
	if(data.slice(0, 16) == '{"table":"trade"') {
		const obj = JSON.parse(data);
		return obj.data.map(trade => [
			trade.trdMatchID,
			trade.price,
			trade.grossValue,
			trade.side,
			fetchTs,
			new Date(trade.timestamp)
		]);
	}
	return null;
}

function orderbookHuobi(fetchTs, instrument, data) {
	const m = /"ch":"market\.(\w+-\w+)\.depth\.step0"/.exec(data);
	return m && [ fetchTs, new Date(fetchTs), 'snapshot', data ];
}

function tickHuobi(fetchTs, instrument, data) {
	const m = /"ch":"market\.(\w+-\w+)\.trade.detail"/.exec(data);
	if(m) {
		const obj = JSON.parse(data);
		return obj.tick.data.map(trade => [
			trade.id,
			trade.price,
			trade.amount,
			trade.direction == 'buy' ? 'Buy' : 'Sell',
			fetchTs,
			new Date(trade.ts)
		]);
	}
	return null;
}

module.exports = {
	BITMEX: {
		orderbook: orderbookBitmex,
		tick: tickBitmex
	},
	HuobiSwap: {
		orderbook: orderbookHuobi,
		tick: tickHuobi
	}
};