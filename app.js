'use strict';

const debug = require('debug')('data-fetcher');
const commander = require('commander');
const Connectors = require('./connectors-async');
const FileWriter = require('./writers/file-writer');

main();

function commaSeparatedList(value, dummyPrev) {
	return value.split(/, *| +/);
}

async function main() {
	const program = new commander.Command();
	program.version('0.1');

	program
		.addOption(new commander.Option('-s, --storage-path <path>', 'path to save fetched data to')
			.default('./storage'))
		.addOption(new commander.Option('-c, --connector <name>', 'exchange connector name')
			.choices(Object.keys(Connectors))
			.makeOptionMandatory())
		.addOption(new commander.Option('-i, --instruments <items>', 'comma separated instrument names')
			.makeOptionMandatory()
			.argParser(commaSeparatedList))
		.addOption(new commander.Option('-I, --incremental', 'subscribe to incremental orderbook updates instead of snapshots'))
		.addOption(new commander.Option('-t, --time-limit <seconds>', 'max data file size in seconds')
			.default(3600, 'one hour'));
		// .addOption(new commander.Option('-s, --size-limit <Mbytes>', 'max data file size in Mbytes')
		// 	.default(10));

	program.parse(process.argv);
	const options = program.opts();
	debug('running with options:', options);

	let lastFlushTS = 0;
	const writers = {};

	async function writeLine(msg, instrument, instrSubst, connector) {
		const now = Date.now();
		if(now - lastFlushTS >= options.timeLimit * 1000) {
			lastFlushTS = now;
			for(const instr of options.instruments) {
				await writers[instr].flushAsync();
				if(connector)
					await connector.resubscribe(instr);
			}
		}
		await writers[instrument].writeAsync(now, options.connector, instrSubst ?? instrument, msg);
	}

	async function flushAll(writers) {
		for(const instrument in writers) {
			debug('calling flushAsync for', instrument, 'writer');
			await writers[instrument].flushAsync();
		}
	}

	try {
		for(const instrument of options.instruments) {
			writers[instrument] = new FileWriter({
				filenameFormat: options.storagePath + `/${options.connector}/${instrument}/data_{ts}.csv`
			});
		}

		const connectorOptions = { incremental: options.incremental };
		// if(options.connector == 'BINANCE') {
		// 	connectorOptions.baseEndpoint = 'wss://stream.binance.com:9443/ws/stream'
		// }
		const connector = new Connectors[options.connector](options.instruments, {
			onReconnect: async (ws) => {
				debug('onReconnect');
				for(const instrument of options.instruments)
					await writeLine('', instrument, '==CONNECTING==', connector);
			},
			onOpen: async (ws) => {
				debug('onOpen');
				for(const instrument of options.instruments)
					await writeLine('', instrument, '==CONNECTED==', connector);
			},
			onUsefulData: writeLine
		}, connectorOptions);

		function onTerminate(signal) {
			debug('gracefully terminating process on signal', signal);
			connector.requestStop('process termination');
		}
		process.on('SIGINT',  onTerminate);
		process.on('SIGTERM', onTerminate);

		await connector.run();

		await flushAll(writers);

	} catch(e) {
		debug('got exception:', e);
		await flushAll(writers);
		process.exit(1);
	}
}
