'use strict';

// MIT License
// Copyright (c) 2020 Vadzim
// https://github.com/vadzim/websocket-iterator

const WebSocket = require('ws');
const debug = require('debug')('websocket-iterator');

function thenableReject(error) {
	return {
		then: (resolve, reject) => reject(error)
	};
}

async function* websocketData(websocket) {
	for await (const { data	} of websocketEvents(websocket)) yield data;
}

function websocketEvents(websocket, { emitOpen = false } = {}) {
	let done = false;
	const values = [];
	const resolvers = [];

	const close = () => {
		done = true;

		while (resolvers.length > 0) resolvers.shift()({
			value: undefined,
			done: true
		});
	};

	const push = data => {
		if (done) return;

		if (resolvers.length > 0) {
			resolvers.shift()(data);
		} else {
			values.push(data);
		}
	};

	const pushError = error => {
		push((0, thenableReject)(error));
		close();
	};

	const pushEvent = event => push({
		value: event,
		done: false
	});

	const next = () => {
		if (values.length > 0) return Promise.resolve(values.shift());
		if (done) return Promise.resolve({
			value: undefined,
			done: true
		});
		return new Promise(resolve => resolvers.push(resolve));
	};

	const initSocket = () => {
		websocket.addEventListener("close", close);
		websocket.addEventListener("error", pushError);
		websocket.addEventListener("message", pushEvent);
	};

	if (websocket.readyState === WebSocket.CONNECTING) {
		websocket.addEventListener("open", event => {
			if (emitOpen) pushEvent(event);
			initSocket();
		});
	} else {
		initSocket();
	}

	const iterator = {
		[Symbol.asyncIterator]: () => iterator,
		next,
		throw: async value => {
			pushError(value);
			if (websocket.readyState === WebSocket.OPEN) websocket.close();
			return next();
		},
		return: async () => {
			close();
			if (websocket.readyState === WebSocket.OPEN) websocket.close();
			return next();
		}
	};
	return iterator;
}

module.exports = { websocketData, websocketEvents };
