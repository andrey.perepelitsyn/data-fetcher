'use strict';

const debug = require('debug')('ws-bitmex-async');
const WebSocket = require('ws');
const WSConnectorAsync = require('./ws-connector-async');

class WSBitmexAsync extends WSConnectorAsync {
	static baseEndpoint = 'wss://ws.bitmex.com/realtime';

	tryMessageAsUseful(msg)	{
		const result = this.tryMessageAsTrade(msg) || this.tryMessageAsOrderBook(msg);
		return result;
	}
	tryMessageAsTrade(msg) {
		const m = /"table":"trade".+"symbol":"(\w+)"/.exec(msg);
		return m && m[1];
	}
	tryMessageAsOrderBook(msg) {
		const m = /"table":"orderBook.+"symbol":"(\w+)"/.exec(msg);
		return m && m[1];
	}
	async onUnclaimedMessage(msg) {
		// debug('onUnclaimedMessage(%s)', msg);
	}
	async subscribe() {
		debug('WSBitmex.subscribe()');
		await this.send({ op: 'subscribe', args: this.instruments.map(instrument => 'trade:'+instrument) });
		await this.send({ op: 'subscribe', args: this.instruments.map(instrument => 'orderBookL2:'+instrument) });
	}
	async run() {
		this.resubscribeTimer = setInterval(this.resubscribe.bind(this), this.options.snapshotOrderbookInterval);
		await super.run();
		clearInterval(this.resubscribeTimer);
	}
	async resubscribe() {
		if(this.ws.readyState === WebSocket.OPEN) {
			await this.send({ op: 'unsubscribe', args: this.instruments.map(instrument => 'orderBookL2:'+instrument) });
			await this.send({ op: 'subscribe', args: this.instruments.map(instrument => 'orderBookL2:'+instrument) });
		}
	}
}

module.exports = WSBitmexAsync;
