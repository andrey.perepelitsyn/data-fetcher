'use strict';

const debug = require('debug')('ws-binance-async');
const WebSocket = require('ws');
const WSBinanceAsync = require('./binance-async');

class WSBinanceFuturesAsync extends WSBinanceAsync {
	static baseEndpoint = 'wss://dstream.binance.com/ws/stream';
	static baseRestEndpoint = 'https://dapi.binance.com/dapi/v1';
}

module.exports = WSBinanceFuturesAsync;
