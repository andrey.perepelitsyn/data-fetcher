'use strict';

const debug = require('debug')('ws-binance-async');
const WebSocket = require('ws');
const WSConnectorAsync = require('./ws-connector-async');
const fetch = require('node-fetch');

class WSBinanceAsync extends WSConnectorAsync {
	static baseEndpoint = 'wss://stream.binance.com:9443/ws/stream';
	static baseRestEndpoint = 'https://api.binance.com/api/v3';
	symbolRE = /"s":"([^"]+)"/;

	tryMessageAsUseful(msg) {
		const result = this.tryMessageAsTrade(msg) || this.tryMessageAsOrderBook(msg);
		return result;
	}
	tryMessageAsTrade(msg) {
		if(msg.indexOf('trade') == -1)
			return null;
		const m = this.symbolRE.exec(msg);
		return m && m[1];
	}
	tryMessageAsOrderBook(msg) {
		if(msg.indexOf('depthUpdate') == -1)
			return null;
		const m = this.symbolRE.exec(msg);
		return m && m[1];
	}
	async subscribe() {
		debug('subscribe()');
		for(const instrument of this.instruments) {
			await this.resubscribe(instrument);
		}
	}
	async resubscribe(instrument) {
		debug('resubscribe(%s)', instrument);
		if(this.ws.readyState == WebSocket.OPEN) {
			await this.send(this.subscribeMessage(instrument));
			const res = await fetch(this.options.baseRestEndpoint + `/depth?symbol=${instrument}&limit=1000`);
			const data = await res.text();
			if(data.length == 0)
				throw new Error("null response");
			await this.handlers.onUsefulData(data, instrument, instrument, this);
			debug('sent snapshot for', instrument);
		}
	}

	subscribeMessage(instrument) {
		return {
			method: 'SUBSCRIBE',
			params: [
				instrument.toLowerCase() + '@trade',
				instrument.toLowerCase() + '@depth@100ms'
			],
			id: this.wsReqId
		};
	};
}

module.exports = WSBinanceAsync;
