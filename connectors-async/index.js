const connectors = {
	BITMEX:         require('./bitmex-async'),
	BINANCESPOT:    require('./binance-async'),
	BINANCEFUTURES: require('./binance-futures-async'),
	HuobiSwap:      require('./huobi-async')
};

module.exports = connectors;
