'use strict';

const zlib = require('zlib');
const WebSocket = require('ws');
const wsIter = require('./websocket-iterator');
const debug = require('debug')('ws-connector-async');

class WSConnectorAsync {
	handlers = {
		onTrade:      async (msg, instrument) => debug('default onTrade()'),
		onOrderBook:  async (msg, instrument) => debug('default onOrderBook()'),
		onUsefulData: async (msg, instrument) => debug('default onUsefulData'),
		onReconnect:  async (ws) => debug('default onReconnect()'),
		onOpen:       async (ws) => debug('default onOpen()')
	};

	options = {
		snapshotOrderbookInterval: 600000,
		heartbeatInterval:           5000,
		timeout:                    30000,
		reconnectPause:              7000,
		reconnectsLimit:               10,
		reconnectsLimitTimeframe:   60000,
		incremental:                false
	};

	constructor(instruments, handlers, options = {}) {
		this.instruments = instruments || [];
		this.handlers = Object.assign(this.handlers, handlers);
		this.options = Object.assign(this.options, options);
		this.options.baseEndpoint ??= this.constructor.baseEndpoint;
		this.options.baseRestEndpoint ??= this.constructor.baseRestEndpoint;
		this.reconnects = [];
		this.stopRequested = false;
		this.wsReqId = 1;
	}

	requestStop(reason) {
		debug('stop requested because of', reason);
		this.stopRequested = true;
		this.ws.close();
	}

	async send(data, options) {
		// debug('send():', data)
		const strData = (typeof data == 'string' || options) ? data : JSON.stringify(data);
		return new Promise((resolve, reject) => {
			this.ws.send(strData, options, (err) => {
				if(err)
					reject(err);
				else
					resolve();
				this.wsReqId++;
			});
		});
	}

	async run() {
		this.lastTimeAlive = Date.now();
		this.heartbeatTimer = setInterval(this.onHearbeat.bind(this), this.options.heartbeatInterval);

		while(!this.stopRequested) {
			// first, do the reconnect timing
			const now = Date.now();
			if(this.reconnects.length >= this.options.reconnectsLimit) {
				this.reconnects = this.reconnects.slice(-this.options.reconnectsLimit);
				if(this.reconnects[0] + this.options.reconnectsLimitTimeframe >= now) {
					// wait for reconnectPause
					debug('too many reconnects so far, waiting for', this.options.reconnectPause, 'ms');
					await new Promise(resolve => setTimeout(resolve, this.options.reconnectPause));
				}
			}

			// then, open websocket
			this.ws = new WebSocket(this.options.baseEndpoint);
			this.reconnects.push(now);
			await this.handlers.onReconnect(this.ws);

			// finally, iterate websocket events
			for await (const event of wsIter.websocketEvents(this.ws, { emitOpen: true })) {
				this.lastTimeAlive = Date.now();
				switch(event.type) {
					case 'open':
						await this.subscribe();
						await this.handlers.onOpen(this.ws);
						break;
					case 'message':
						const msg = (typeof event.data != 'string') ? zlib.gunzipSync(event.data).toString() : event.data.toString();
						let instrument;
						if(instrument = this.tryMessageAsUseful(msg))
							await this.handlers.onUsefulData(msg, instrument, instrument, this);
						else if(instrument = this.tryMessageAsTrade(msg))
							await this.handlers.onTrade(msg, instrument);
						else if(instrument = this.tryMessageAsOrderBook(msg))
							await this.handlers.onOrderBook(msg, instrument);
						else
							await this.onUnclaimedMessage(msg);
						break;
					default:
						debug("this shouldn't happen! ws event:", event);
				}
				if(this.stopRequested)
					break;
			}
			debug('connection lost, reconnecting...');
		}
		clearInterval(this.heartbeatTimer);
		debug('heartbeat timer canceled, run() returns');
	}

	onHearbeat() {
		const now = Date.now();

		if(now - this.lastTimeAlive > 2 * this.options.heartbeatInterval) {
			debug('stale connection detected in state', this.ws.readyState);
			this.ws.terminate();
		}
	}

	async onUnclaimedMessage(msg) {
		debug('unclaimed message:', msg);
	}

	async subscribe() {
		debug('default subscribe()');
	}

	// methods tryMessageAs... should return instrument name if msg is fits, or null

	tryMessageAsTrade(msg) {
		throw new ReferenceError('tryMessageAsTrade() is abstract!');
	}

	tryMessageAsOrderBook(msg) {
		throw new ReferenceError('tryMessageAsOrderBook() is abstract!');
	}

	tryMessageAsUseful(msg) {
		throw new ReferenceError('tryMessageAsUseful() is abstract!');
	}
}

module.exports = WSConnectorAsync;
