'use strict';

const debug = require('debug')('ws-huobi-async');
const WebSocket = require('ws');
const WSConnectorAsync = require('./ws-connector-async');

class WSHuobiAsync extends WSConnectorAsync {
	static baseEndpoint = 'wss://api.hbdm.com/swap-ws';

	tryMessageAsUseful(msg)	{
		const result = this.tryMessageAsTrade(msg) || this.tryMessageAsOrderBook(msg);
		return result;
	}
	tryMessageAsTrade(msg) {
		const m = /"ch":"market\.(\w+-\w+)\.trade.detail"/.exec(msg);
		return m && m[1];
	}
	tryMessageAsOrderBook(msg) {
		const m = /"ch":"market\.(\w+-\w+)\.depth\./.exec(msg);
		return m && m[1];
	}
	async onUnclaimedMessage(msg) {
		try {
			const obj = JSON.parse(msg);
			if(obj.ping)
				await this.send({ pong: obj.ping });
			else
				debug('unclaimed object message:', obj);
		} catch(e) {
			debug('onUnclaimedMessage(%s):', msg, e);
			throw e;
		}
	}
	obChannelName(instrument) {
		const depthSuffix = this.options.incremental ? "size_150.high_freq" : "step0";
		return `market.${instrument}.depth.${depthSuffix}`;
	}
	async subscribe() {
		debug('WSHuobi.subscribe()');
		for(const instrument of this.instruments) {
			await this.send({
				sub: `market.${instrument}.trade.detail`,
				id: 'trade-sub-'+Date.now()
			});
			await this.send({
				sub: this.obChannelName(instrument),
				data_type: 'incremental',
				id: 'depth-sub-'+Date.now()
			});
		}
	}
	async resubscribe(instrument) {
		debug('resubscribe:', this.obChannelName(instrument));
		if(this.ws.readyState === WebSocket.OPEN) {
			
			const id = 'depth-unsub-' + Date.now();
			await this.send({
				unsub: this.obChannelName(instrument),
				data_type: 'incremental',
				id
			});
			// TODO: somehow we should wait here for unsubscribe confirmation
			await this.send({
				sub: this.obChannelName(instrument),
				data_type: 'incremental',
				id: 'depth-sub-'+Date.now()
			});
		}
	}
}

module.exports = WSHuobiAsync;