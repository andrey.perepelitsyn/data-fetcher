'use strict';

const debug = require('debug')('base-writer');

class BaseWriter {
	options = {};

	constructor(options) {
		this.options = Object.assign(this.options, options);
	}

	write(ts, connectorName, instrument, data, callback) {
		throw new ReferenceError('write() is abstract!');
	}

	async writeAsync(ts, connectorName, instrument, data) {
		return new Promise((resolve, reject) => {
			this.write(ts, connectorName, instrument, data, (err) => {
				if(err)
					reject(err);
				else
					resolve();
			});
		});
	}

	flush(callback) {
		if(typeof callback == 'function')
		callback();
	}

	async flushAsync() {
		return new Promise((resolve, reject) => {
			this.flush(resolve);
		})
	}
}

module.exports = BaseWriter;
