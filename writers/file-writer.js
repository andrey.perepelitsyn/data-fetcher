'use strict';

const fs = require('fs');
const Path = require('path');
const debug = require('debug')('file-writer');
const BaseWriter = require('./base-writer');

class FileWriter extends BaseWriter {
	fileStream = null;

	constructor(options) {
		super(options);
		this.options = Object.assign(this.options, options);
		this.options.filenameFormat ??= '{connectorName}/{instrument}/data_{ts}.csv';
	}

	write(ts, connectorName, instrument, data, callback) {
		const line = `${ts}\t${connectorName}\t${instrument}\t${data}\n`;
		if(this.fileStream) {
			this.fileStream.write(line);
			callback();
		} else {
			debug('fileStream is null, opening new one');
			this.openFileStream(ts, connectorName, instrument, (err) => {
				if(err)
					callback(err);
				else {
					this.fileStream.write(line);
					callback();
				}
			});
		}
	}

	flush(callback) {
		debug('flush()');
		if(this.fileStream) {
			this.fileStream.on('close', () => {
				debug('flush() complete');
				callback();
			});
			this.fileStream.end();
			this.fileStream = null;
		} else {
			debug('already flushed');
			callback();
		}
	}

	openFileStream(ts, connectorName, instrument, callback) {
		const templateParams = { ts, connectorName, instrument };
		const path = this.options.filenameFormat.replace(
			/\{(\w+)\}/g,
			(match, paramName) => templateParams[paramName]);

		const dirPath = Path.dirname(path);
		if(dirPath == '')
			this.createFileStream(path, callback);
		else {
			fs.mkdir(dirPath, { recursive: true }, (err) => {
				if(err)
					callback(err);
				else {
					this.createFileStream(path, callback);
				}
			});
		}
	}

	createFileStream(path, callback) {
		this.fileStream = fs.createWriteStream(path, {
			flags: 'wx',
			encoding: 'utf8',
			emitClose: true
		});
		this.fileStream.on('ready', () => {
			debug('stream is ready:', path);
			callback();
		});
		this.fileStream.on('error', (err) => {
			debug('unable to open stream');
			callback(err);
			this.fileStream.on('error', this.onFileStreamError.bind(this));
		});
	}

	onFileStreamError(err) {
		debug(err);
	}
}

module.exports = FileWriter;
