#!/bin/bash

storage_root=${1:-./storage}
compress=xz
verbosity=-v

for data_path in $storage_root/*/*
do
	echo $data_path
	for data_file in `ls -1 $data_path/*.csv | head -n -1`
	do
		$compress $verbosity $data_file
	done
done
